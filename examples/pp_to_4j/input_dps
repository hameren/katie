# Particles are encoded as follows:
# ve   ve~   e-   e+   u u~ d d~
# vmu  vmu~  mu-  mu+  c c~ s s~
# vtau vtau~ tau- tau+ t t~ b b~
# g H

# Number of groups
Ngroup = 2

# Number of final-state particles in each group
Nfinst = 2 2

# List of processes. 
#
# If you wish to sum over pdfs for processes with equivalent matrix elements,
# then you need to set
   partlumi = combined
# in this file. This is NOT SUITABLE to create event files for parton showers.
# Indicating quarks with q and r implies summation over initial-state pdfs.
# Indicating quarks with u d u~ d~ implies summation within up/down-type
# and within regular/anti-type, so for the case of electro-weak interactions.
# The cross section for each process is multiplied with the quantity "factor"
# on each process-line, for the summation over final-state partons.
# The next quantity indicates to which groups the process contributes in the
# case of multi-parton-interactions.
#
# The last quantity sets the number of desired non-QCD couplings in the
# amplitude, respectively of type:  electro-weak  higgs-gluon  higgs-photon
# Of course the necessary interactions need to be switched on in the
# user module.

process = g g  -> g g   factor = 1     groups = 1 2  pNonQCD = 0 0 0
process = g g  -> q q~  factor = Nf    groups = 1 2  pNonQCD = 0 0 0
process = g q  -> q g   factor = 1     groups = 1 2  pNonQCD = 0 0 0
process = q g  -> q g   factor = 1     groups = 1 2  pNonQCD = 0 0 0
process = q q  -> q q   factor = 1     groups = 1 2  pNonQCD = 0 0 0
process = q r  -> q r   factor = 1     groups = 1 2  pNonQCD = 0 0 0
process = q r~ -> q r~  factor = 1     groups = 1 2  pNonQCD = 0 0 0
process = q q~ -> g g   factor = 1     groups = 1 2  pNonQCD = 0 0 0
process = q q~ -> q q~  factor = 1     groups = 1 2  pNonQCD = 0 0 0
process = q q~ -> r r~  factor = Nf-1  groups = 1 2  pNonQCD = 0 0 0

# Collinear pdf set. Also in TMD-case, alphaStrong is taken from there.
lhaSet = MSTW2008nlo68cl

#Choose which initial states must be off-shell
#offshell = 1 1  # eg.  g* g* -> ... 
#offshell = 0 1  # eg.  g  g* -> ...
#offshell = 1 0  # eg.  g* g  -> ...
offshell = 0 0  # eg.  g  g  -> ...

# Paths and files related to TMDs must be out-commented if you do not
# want to use them.
# Directory where TMDs can be found:
tmdTableDir = /home/user0/kTfac/tables/krzysztof02/
# TMD grid files. You can change (update) tmdTableDir between them.
# All of them will be loaded, so don't put more than necessary.
tmdpdf = g  KMR_gluon.dat
tmdpdf = u  KMR_u.dat
tmdpdf = u~ KMR_ubar.dat
tmdpdf = d  KMR_d.dat
tmdpdf = d~ KMR_dbar.dat
tmdpdf = s  KMR_s.dat
tmdpdf = s~ KMR_sbar.dat
tmdpdf = c  KMR_c.dat
tmdpdf = c~ KMR_cbar.dat
tmdpdf = b  KMR_b.dat
tmdpdf = b~ KMR_bbar.dat

# sigma effective for dps
sigma_eff = 15d6

# Number of parton flavors.
Nflavors = 5

# Number of nonzero-weight phase space points to be spent on optimization.
Noptim = 100,000

# Center of mass energy. Must be larger than zero.
Ecm = 7000

# Phase space cuts. Examples:
# {pT|2|} is the pT of final state 2.
# {pT|2|1,2,4} is the 2nd-hardest pT of final states 1,2,4.
# {pT|1+4|} is the pT of the sum of final-state momenta 1 and 4.
# {deltaR|1,2|} is the delta-R between final states 1 and 2.
# The value of  rapidity  and  pseudoRap  can be negative.
# Further available are  ET  mass  deltaPhi .
cut = {deltaR|1,2|} > 0.5
cut = {deltaR|1,3|} > 0.5
cut = {deltaR|1,4|} > 0.5
cut = {deltaR|2,3|} > 0.5
cut = {deltaR|2,4|} > 0.5
cut = {deltaR|3,4|} > 0.5
cut = {pT|1|1,2,3,4} > 50
cut = {pT|2|1,2,3,4} > 50
cut = {pT|3|1,2,3,4} > 20
cut = {pT|4|1,2,3,4} > 20
cut = {rapidity|1|} > -4.7
cut = {rapidity|2|} > -4.7
cut = {rapidity|3|} > -4.7
cut = {rapidity|4|} > -4.7
cut = {rapidity|1|} <  4.7
cut = {rapidity|2|} <  4.7
cut = {rapidity|3|} <  4.7
cut = {rapidity|4|} <  4.7
# Renormalization/factorization scale, for both scatterings separately.
scale = entry 1 ({pT|1|}+{pT|2|})/2
scale = entry 2 ({pT|3|}+{pT|4|})/2

# Each group needs its own inclusive cuts for phase space optimization.
cut = group 1 {deltaR|1,2|} > 0.5
cut = group 1 {pT|1|} > 20
cut = group 1 {pT|2|} > 20
cut = group 1 {rapidity|1|} > -4.7
cut = group 1 {rapidity|2|} > -4.7
cut = group 1 {rapidity|1|} <  4.7
cut = group 1 {rapidity|2|} <  4.7
scale = group 1 ({pT|1|}+{pT|2|})/2

# The following cuts happen to be the same as the previous ones,
# because for just jets the groups are identical.
cut = group 2 {deltaR|1,2|} > 0.5
cut = group 2 {pT|1|} > 20
cut = group 2 {pT|2|} > 20
cut = group 2 {rapidity|1|} > -4.7
cut = group 2 {rapidity|2|} > -4.7
cut = group 2 {rapidity|1|} <  4.7
cut = group 2 {rapidity|2|} <  4.7
scale = group 2 ({pT|1|}+{pT|2|})/2

# Masses and widths
mass = Z   91.1882  2.4952
mass = W   80.419   2.21
mass = H  125.0     0.00429
mass = t  173.5
# Interactions
switch = withQCD   Yes
switch = withQED   No
switch = withWeak  No
switch = withHiggs No
switch = withHG    No
# Couplings. You can set either alphaEW or Gfermi
coupling = Gfermi 1.16639d-5
